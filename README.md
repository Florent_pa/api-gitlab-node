# API Gitlab Node

Ce projet a été réalisé pour prendre en main l'outil qu'est Node. L'objectif de ce projet est double :
- Construire une architecture simple utilisant les spécificités du langage (les promesses).
- Utiliser certaines librairies communes pour maintenir son code (un linter, de la documentation, des tests unitaires).

## La partie linter

Pour rendre le code "conforme" au sein des différents fichiers (sources et tests), j'utilise la librairie eslint.
La configuration d'eslint se trouve dans le fichier .eslintrc.json.
Pour exécuter le linter, il faut lancer l'une des commandes suivantes :
```bash
npm run lint # vérifie que les conventions d'écriture sont conformes
npm run fixlint # Règle les "problèmes simples" de convention d'écriture avant de faire un retour sur ce qui ne va pas.
```

Pour plus d'information sur les plug-ins utilisés et sur le script lancé, voir le fichier package.json.

## La partie test

Les tests sont présents dans le dossier test/.
Pour les exécuter, il faut lancer la commande suivante :
```bash
npm test # ou npm run test
```

Voir le fichier package.json pour plus d'information sur le script lancer.
