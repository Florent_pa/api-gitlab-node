/** @module api/external/Gitlab */

const fetch = require('node-fetch')

/** Class representing a Gitlab user */
class User {
    /**
    * username Field
    * @constant
    * @private
    */
    #username

    /**
    * webUrl Field
    * @constant
    * @private
    */
    #webUrl

    /**
    * Builds the Gitlab user
    * @param {json} json - JSON object containing Gitlab API information
    */
    constructor (json) {
      const fieldsRequired = [
        'username',
        'web_url'
      ]
      for (const elt in fieldsRequired) {
        if (!(fieldsRequired[elt] in json)) {
          throw new TypeError('Sorry : User object was not created - invalid parameter')
        }
      }
      this.#username = json.username
      this.#webUrl = json.web_url
    }

    /**
    * Returns a representation of the object in JSON format
    * @return The object in JSON format
    */
    toJSON () {
      return {
        username: this.#username,
        web_url: this.#webUrl
      }
    }
}

/** Class wrapping gitlab API. */
class API {
  /**
    * Get information about users of the Gitlab API
    * @param {string} username - The usernmae to fetch gitlab information
    * @return {json} - A json with gitlab User information
    * @static
    */
  static async getUser (username) {
    const response = await fetch('https://gitlab.com/api/v4/users?username=' + username)
    const data = await response.json()
    return data
  }
}

module.exports.User = User
module.exports.API = API
