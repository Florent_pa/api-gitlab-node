const describe = require('mocha').describe
const it = require('mocha').it
const assert = require('assert')
const { FetchError } = require('node-fetch')
const User = require('../../../src/api/external/gitlab.js').User
const API = require('../../../src/api/external/gitlab.js').API

describe('gitlab API', () => {
  describe('User class', () => {
    it('Should create a user with the right parameters', () => {
      let rightParameter = {
        username: 'username',
        web_url: 'web_url'
      }
      let user = new User(rightParameter)
      assert.strictEqual(true, user instanceof User)

      rightParameter = {
        username: 'username',
        web_url: 'web_url',
        injection: 'injection'
      }
      user = new User(rightParameter)
      assert.strictEqual(true, user instanceof User)
    })
    it('Should throw an error when a wrong parameter is given', () => {
      let badParameter = {}
      try {
        const user = new User(badParameter) // Retourne un TypeError
        throw new Error(user, ' instanciate : should\'nt working')
      } catch (e) {
        if (e.name === Error) {
          throw new Error(e)
        }
        assert.strictEqual(true, e instanceof TypeError)
        assert.strictEqual('Sorry : User object was not created - invalid parameter', e.message)
      }

      badParameter = {
        injection: 'injection'
      }
      try {
        const user = new User(badParameter) // Retourne un TypeError
        throw new Error(user, ' instanciate : should\'nt working')
      } catch (e) {
        if (e.name === Error) {
          throw new Error(e)
        }
        assert.strictEqual(true, e instanceof TypeError)
        assert.strictEqual('Sorry : User object was not created - invalid parameter', e.message)
      }
    })
    it('Should return a JSON object of the User', () => {
      const rightParameter = {
        username: 'username',
        web_url: 'web_url'
      }
      assert.strictEqual(JSON.stringify(new User(rightParameter)), JSON.stringify(new User(rightParameter)))
      assert.strictEqual(JSON.stringify(rightParameter), JSON.stringify(new User(rightParameter)))
    })
    it('Should be different when creating two users with the same correct parameters.', () => {
      const rightParameter = {
        username: 'username',
        web_url: 'web_url'
      }
      const user1 = new User(rightParameter)
      const user2 = new User(rightParameter)
      assert.strictEqual(false, user1 === user2) // Deux références d'objets différents
    })
  })

  describe('API Object', function () {
    it('Should return JSON User API and check need fields', function (done) {
      const needFields = [
        'username',
        'web_url'
      ]
      API.getUser('Florent_pa')
        .then(jsonData => {
          for (const elt of needFields) {
            assert.strictEqual(true, elt in jsonData[0])
          }
          done()
        })
        .catch(error => {
          const errorAllow = 'request to https://gitlab.com/api/v4/users?username=florent_pa failed, reason: getaddrinfo EAI_AGAIN gitlab.com'
          if (error.message === errorAllow) {
            throw new FetchError('You are not connected')
          }
          done(error)
        })
        .catch(error => {
          done(error)
        })
    })
    it('Should catch error with invalid user', function (done) {
      const needFields = [
        'username',
        'web_url'
      ]
      API.getUser('')
        .then(jsonData => {
          for (const elt of needFields) {
            assert.strictEqual(false, elt in jsonData)
          }
          assert.strictEqual(true, 'message' in jsonData)
          done()
        })
        .catch(error => {
          const errorAllow = 'request to https://gitlab.com/api/v4/users?username= failed, reason: getaddrinfo EAI_AGAIN gitlab.com'
          if (error.message === errorAllow) {
            throw new FetchError('You are not connected')
          }
          done(error)
        })
        .catch(error => {
          done(error)
        })
    })
  })
})
